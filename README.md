This repository exists to test our server and runners are connected, and working as expected.

This just has a simple CI, which is deployed as part of the playbooks and then a test script will trigger the pipeline to verify it works.
